//package org.example.configuration;
//
//import org.example.handler.ChatWebSocketHandler;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.socket.WebSocketHandler;
//import org.springframework.web.socket.config.annotation.EnableWebSocket;
//import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
//import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
//
//@Configuration
//@EnableWebSocket
//public class WebSocketConfiguration implements WebSocketConfigurer {
//
//    private final static String PARKING_ENDPOINT = "/parking";
//
//    @Override
//    public void registerWebSocketHandlers(WebSocketHandlerRegistry webSocketHandlerRegistry) {
//        webSocketHandlerRegistry.addHandler(getChatWebSocketHandler(), PARKING_ENDPOINT)
//                .setAllowedOrigins("*");
//    }
//
//    @Bean
//    public WebSocketHandler getChatWebSocketHandler(){
//        return new ChatWebSocketHandler();
//    }
//}
