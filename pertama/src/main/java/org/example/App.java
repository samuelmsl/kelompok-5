package org.example;

import org.json.JSONObject;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        App newApp = new App();
        newApp.parse();

    }

    public void parse() {

        JSONObject myObject = new JSONObject();
        myObject.put("name", "admin");
        System.out.println(myObject.toString());

        String jsonString = "{\"name\": \"admin\"}";
        JSONObject myResp = new JSONObject(jsonString);
        System.out.println(myResp.getString("name"));


    }

}
